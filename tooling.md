# Tooling

## CSV Tools

### Basic CSV Processing

#### Recommended Tools

These are my go to CSV processing tools. `xsv` is easier to use and is generally enough. However `miller` provides more powerful features for statistical analysis and data regularization.

* [xsv](https://github.com/BurntSushi/xsv): A fast CSV command line toolkit written in Rust.
* [miller](https://github.com/johnkerl/miller): Miller is like awk, sed, cut, join, and sort for name-indexed data such as CSV, TSV, and tabular JSON

#### Other CSV Tools

Some other interesting options for CSV processing. `csvq` looks particularly interesting.

* [csvq](https://github.com/mithrandie/csvq): SQL-like query language for csv ([docs](https://mithrandie.github.io/csvq/))
* [csvkit](https://github.com/wireservice/csvkit): A suite of utilities for converting to and working with CSV, the king of tabular file formats. ([docs](https://csvkit.readthedocs.io/en/latest/))
* [csvtk](https://github.com/shenwei356/csvtk): A cross-platform, efficient and practical CSV/TSV toolkit in Golang

### Manipulation and Linting

These tools should help you clean up and normalize your data, possibly to a larger degree than `miller` does.

* [csvlint.rb](https://github.com/theodi/csvlint.rb): A ruby gem to support validating CSV files to check their syntax and contents.
* [csvprintf](https://github.com/archiecobbs/csvprintf): Simple CSV file parser for the UNIX command line
* [csv-groupby](https://github.com/sflanaga/csv-groupby): Do select-group-by on csv and other text files
* [csv-guillotine](https://github.com/forbesmyester/csv-guillotine): CSV's often have metadata at top before data headers. This removes it.
* [CSVfix](https://neilb.bitbucket.io/csvfix/): A command-line tool specifically designed to deal with CSV data.

### Visualization

If you need to graph your data these might come in handy.

* [visidata](https://github.com/saulpw/visidata): A terminal spreadsheet multitool for discovering and arranging data ([project page](https://www.visidata.org/))
* [raw](https://github.com/densitydesign/raw): The missing link between spreadsheets and data visualization ([project page](https://rawgraphs.io/))
* [gnuplot](http://www.gnuplot.info/): A portable command-line driven graphing utility.

**NOTE**: Uploading data to web based tools like `raw` may be a **security, privacy, and regulatory issue** depending on the sensitivity and sources of your dataset.

## Further Resources

If somehow, none of the above works out, here are some additional places to look.

* [AwesomeCSV](https://github.com/secretGeek/AwesomeCSV): 🕶️A curated list of awesome tools for dealing with CSV.
* [awesome-csv-tools](https://github.com/brasilikum/awesome-csv-tools): Awesome Tools for a Not-So-Awesome File Format
