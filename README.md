# Developing Life

Just a general place to learn about and practice development of all sorts.

## Topics

* [Tooling](tooling.md) - The programs we use to get work done.

# Further Reading

Awesome-* repos are a great source of curated data. The below are meta repos i.e. curated lists of links to other curated lists.

* [awesome-awesome](https://github.com/emijrp/awesome-awesome.git): A curated list of awesome curated lists of many topics.
* [awesome-awesome-awesome](https://github.com/jonatasbaldin/awesome-awesome-awesome): Awesome list of repositories of awesome lists 🤷‍♀️
* [awesome-awesome-awesome-awesome](https://github.com/sindresorhus/awesome-awesome-awesome-awesome): A curated list of awesome lists of awesome lists.
* [awesome-awesomeness](https://github.com/bayandin/awesome-awesomeness): A curated list of awesome awesomeness
